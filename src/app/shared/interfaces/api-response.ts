export interface ApiResponse {
    result?:boolean
    message?:string 
    status?:string 
    icon?:string 
    data?:any
}
