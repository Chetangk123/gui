export interface Institute {
    name:string,
    moto:string,
    number:string,
    email:string,
    website:string,
    address:string,
    country:string,
    logo: File,
}
